<?php
/*
	$mainlang = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
	$mainmenu = App\Menu::where('position', 'top')->orWhere('position', 'all')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

	Route::redirect('/', '/'.$mainlang->code.'/'.$mainmenu->slug, 301);
	Route::get('/{lang}/{slug}/{attr?}/{param?}', 'HomeController@index');
*/

$currentLangCount = App\Language::where('code', Request::segment(1))->where('deleted', 'no')->where('status', 'active')->count();
if ($currentLangCount == 1) {
	$currentLang = App\Language::where('code', Request::segment(1))->where('deleted', 'no')->where('status', 'active')->first();
}else{
	$currentLang = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
}

$mainMenu = App\Menu::where(
	function ($query){
		$query->where('position', 'all')->orWhere('position', 'top');
	}
)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

if ($currentLangCount != 1) {
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: /'.$currentLang->code.'/'.$mainMenu->variable->slug);
	exit();
}

Route::redirect('/', '/'.$currentLang->code.'/'.$mainMenu->variable->slug, 301);
Route::get('/{lang}/{slug}/{attr?}/{param?}', 'HomeController@index');