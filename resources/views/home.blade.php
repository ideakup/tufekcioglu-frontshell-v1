@extends('layouts.app')

@section('content')
    
    <?php
        $_masterColContentClasses = '';
        $_rowContentClasses = '';
        $_colContentClasses = '';
        $_rowBGImageUrlStyle = '';

        if ($menu->asidevisible == 'yes'){
            $_masterColContentClasses = 'col-10 col-sm p-left-y5'; 
        } else {
            if ($mainmenu->variableLang(Request::segment(1))->slug == $menu->variableLang(Request::segment(1))->slug) {
                $_masterColContentClasses = 'col-md-10 offset-md-1 col-sm-12'; 
            } else {
                $_masterColContentClasses = 'col-12 col-sm'; 
            }
        }
    ?>

    <div class='{{ $_masterColContentClasses }} master-col-content'>
        
        @if ($mainmenu->variableLang(Request::segment(1))->slug != $menu->variableLang(Request::segment(1))->slug)
            <div class='row'>
                <div class='col-12'>
                    <h2> {{ $menu->variableLang(Request::segment(1))->name }} </h2>
                </div>
            </div>
        @endif
        
        @foreach ($menu->content as $cont)

            <?php
                if (empty($cont->variableLang(Request::segment(1)))) {
                    $contVariable = $cont->variable;
                }else{
                    $contVariable = $cont->variableLang(Request::segment(1));
                }

                if ($contVariable->row == 'normal'){
                    $_rowContentClasses = 'row ';
                    $_colContentClasses = '';
                } else if ($contVariable->row == 'full'){ 
                    $_rowContentClasses = 'row-full ';
                    $_colContentClasses = 'p-lr-0 '; 
                }

                if (is_null($contVariable->bgimageurl)){
                    $_colContentClasses .= '';
                    $_rowBGImageUrlStyle = '';
                } else { 
                    $_colContentClasses .= 'bg-image-container ';
                    $_rowBGImageUrlStyle .= 'background-image: url("'.env('APP_UPLOAD_PATH_V3').'xlarge/'.$contVariable->bgimageurl.'"); ';

                    if (!is_null($contVariable->height)){
                        $_colContentClasses .= '';
                        $_rowBGImageUrlStyle .= 'height: '.$contVariable->height.'px;';
                    }
                }
            ?>

            <div class="{{ $_rowContentClasses }}">
                <div class="col-12 margin-b-35 {{ $_colContentClasses }}" style="{{$_rowBGImageUrlStyle}}">
                    @if ($cont->type == 'text')
                            {!! $contVariable->content !!}
                    @elseif ($cont->type == 'photo')
                        @if ($contVariable->props == 'responsive')
                            <p style="text-align: center;"><img class="img-fluid" src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $contVariable->content }}"></p>
                        @else
                            <p style="text-align: center;"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $contVariable->content }}"></p>
                        @endif 
                    @elseif ($cont->type == 'photogallery')
                        @foreach ($cont->photogallery as $img)
                            <div class="
                                @if ($contVariable->props == 'col-2x')
                                    col-6
                                @elseif ($contVariable->props == 'col-3x')
                                    col-4
                                @elseif ($contVariable->props == 'col-4x')
                                    col-3
                                @endif
                            " style="float: left; padding: 15px;">
                                <a href="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $img->url }}" data-lightbox="lightbox-{{$cont->id}}" data-title="{{ $img->name }}">
                                    <img class="img-fluid" src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail2x/{{ $img->url }}" alt="{{ $img->name }}">
                                </a>
                            </div>
                        @endforeach

                    @elseif ($cont->type == 'link')
                        <p style="text-align: center;">
                            <a href="{{ $contVariable->content }}" {{ ($contVariable->props == 'external') ? 'target="_blank"' : '' }} class="btn btn-primary btn-font-size">
                                @if (Request::segment(1) == 'tr')
                                    Daha Fazla Bilgi Al
                                @elseif(Request::segment(1) == 'en')
                                    Read More
                                @elseif(Request::segment(1) == 'ru')
                                    Read More
                                @endif
                            </a>
                        </p>
                    @endif

                </div>
            </div>

        @endforeach

    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) { f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e) }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) { var u = (e.c.width(), jQuery(window).height()); if (void 0 != e.fullScreenOffsetContainer) { var c = e.fullScreenOffsetContainer.split(","); if (c) jQuery.each(c, function(e, i) { u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0)) } f = u } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({ height: f })
            } catch (d) { console.log("Failure at Presize of Slider:" + d) }
        };
    </script>
    <script type="text/javascript">
        var revapi2,
            tpj;
        (function() {
            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
            else onLoad();

            function onLoad() {
                if (tpj === undefined) { tpj = jQuery; if ("off" == "on") tpj.noConflict(); }
                if (tpj("#rev_slider").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider");
                } else {
                    revapi2 = tpj("#rev_slider").show().revolution({
                        sliderType: "standard",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "vertical",
                            mouseScrollNavigation: "on",
                            mouseScrollReverse: "default",
                            onHoverStop: "on",
                            touch: {
                                touchenabled: "on",
                                touchOnDesktop: "off",
                                swipe_threshold: 75,
                                swipe_min_touches: 50,
                                swipe_direction: "vertical",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "hesperiden",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: false,
                                tmp: '',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                }
                            }
                        },
                        responsiveLevels: [1240, 1024, 778, 480],
                        visibilityLevels: [1240, 1024, 778, 480],
                        gridwidth: [1200, 1240, 800, 720],
                        gridheight: [600, 600, 920, 920],
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner2",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }; /* END OF revapi call */
            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
@endsection