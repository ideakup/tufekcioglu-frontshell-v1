<header class="@if($__headertheme == 'home') slider-background @endif">
    <div class="container container-menu-padding">
        
        <style type="text/css">
            .langTool{
                /*border: 1px solid #000;*/
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 9999;

                font-size: 12px;
                text-align: right;
                line-height: 20px;
            }

            @if($__headertheme == 'home')
                .langLink{
                    color: #FFF;
                    font-size: 12px;
                }

                .langLink:hover {
                    color: #b81521;
                    text-decoration: none;
                }
            @else
                .langLink{
                    color: #000;
                    font-size: 12px;
                }

                .langLink:hover {
                    color: #b81521;
                    text-decoration: none;
                }
            @endif
        </style>
        
        <div class="container container-menu-height container-menu-padding">
            
            <nav class="navbar navbar-expand-lg @if($__headertheme == 'home') navbar-dark @else navbar-light bg-light @endif p-top-20">

                @php
                    $segments = '';
                    for ($i=1; $i < count(Request::segments()); $i++) { 
                        $segments = $segments.'/'.Request::segments()[$i];
                    }
                @endphp

                <div class="langTool">
                    @foreach ($langs as $lng)
                        <a class="langLink" href="{{ url($lng->code.$segments) }}">
                            {{ $lng->name }}
                        </a>
                        @if (!$loop->last)
                            <span class="langLink">|</span>
                        @endif
                    @endforeach
                </div>

                <a class="navbar-brand " href="{{ url('/') }}">
                    <img src="@if($__headertheme == 'home') {{ url('images/logo-white-'.Request::segment(1).'.svg') }} @else {{ url('images/logo-black-'.Request::segment(1).'.svg') }} @endif" height="100" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <style type="text/css">
                    .dropdown-submenu {
                      position: relative;
                    }

                    .dropdown-submenu a::after {
                      transform: rotate(-90deg);
                      position: absolute;
                      right: 6px;
                      top: .8em;
                    }

                    .dropdown-submenu .dropdown-menu {
                      top: 0;
                      left: 100%;
                      margin-left: .1rem;
                      margin-right: .1rem;
                    }

                    .dropright-menu {
                        top: 100% !important;
                        left: 0 !important;
                    }

                    .dropdown-item{
                        padding: 0.25rem 0.5rem;
                    }
                </style>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto ust-menu @if($__headertheme == 'home') nav-menu-color @endif">
                        @php $i = 0; @endphp
                        @foreach ($topmenus as $menuitem)
                            @php
                                if (empty($menuitem->variableLang($lang))) {
                                    $menuitemVariable = $menuitem->variable;
                                }else{
                                    $menuitemVariable = $menuitem->variableLang($lang);
                                }
                            @endphp

                            @include('partials.headermenu')
                            @if (!$loop->last)
                                <li role="separator" class="divider">&nbsp;|&nbsp;</li>
                            @endif
                        @endforeach
                      
                    </ul>
                </div>

            </nav>
            
        </div>
    </div>

    @include('partials.slidertype')
</header>