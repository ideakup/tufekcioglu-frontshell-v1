@if (empty($submenu))
    @if ($menuitem->subMenuTop->count() > 0)
    	@php $pp++; @endphp

		<div class="panel">
			
			<li>
	            <a class="nav-link dropdown-toggle" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapse{{$menuitem->variableLang($lang)->slug}}" aria-expanded="true" aria-controls="collapse{{$menuitem->variableLang($lang)->slug}}">
	                {{ $menuitem->variableLang($lang)->title }} {{-- $menuitem->subMenuTop->count().' * '.$pp --}}
	            </a>
	        </li>

            @if ($menuitem->subMenuTop->count() > 0)
				<ul class="aside-menu-panel panel-collapse collapse in" id="collapse{{$menuitem->variableLang($lang)->slug}}" role="tabpanel">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @if ($menuitem->position != 'aside')
                            @include('partials.asidebarmenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif

        </div>


    @else
		<li>
            @if (!is_null($menuitem->variableLang($lang)))
                @if ($menuitem->type == 'content')
                    <a class="nav-link" href="{{ url($lang.'/'.$menuitem->variableLang($lang)->slug) }}">
                        {{ $menuitem->variableLang($lang)->title }}
                    </a>
                @elseif ($menuitem->type == 'link' && !is_null($menuitem->variableLang($lang)->stvalue))
                    <a class="nav-link" href="{{ json_decode($menuitem->variableLang($lang)->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang($lang)->stvalue)->target }}">
                        {{ $menuitem->variableLang($lang)->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @endif
            @endif
        </li>
    @endif
@else
    @if ($menuitem->subMenuTop->count() > 0)
    	@php $pp++; @endphp
        <div class="panel">

			<li>
	            <a class="nav-link dropdown-toggle" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapse{{$menuitem->variableLang($lang)->slug}}" aria-expanded="true" aria-controls="collapse{{$menuitem->variableLang($lang)->slug}}">
	                {{ $menuitem->variableLang($lang)->title }} {{-- $menuitem->subMenuTop->count().' ? '.$pp --}}
	            </a>
	        </li>

            @if ($menuitem->subMenuTop->count() > 0)
				<ul class="aside-menu-panel panel-collapse collapse in" id="collapse{{$menuitem->variableLang($lang)->slug}}" role="tabpanel">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @if ($menuitem->position != 'aside')
                            @include('partials.asidebarmenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif

        </div>
    @else
    	@php $pp++; @endphp
        @if (!is_null($menuitem->variableLang($lang)))
    		<li>
                @if ($menuitem->type == 'content')
                    <a class="nav-link" href="{{ url($lang.'/'.$menuitem->variableLang($lang)->slug) }}">
                        {{ $menuitem->variableLang($lang)->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @elseif ($menuitem->type == 'link' && !is_null($menuitem->variableLang($lang)->stvalue))
                    <a class="nav-link" href="{{ json_decode($menuitem->variableLang($lang)->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang($lang)->stvalue)->target }}">
                        {{ $menuitem->variableLang($lang)->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @endif
	        </li>
        @endif
    @endif
@endif