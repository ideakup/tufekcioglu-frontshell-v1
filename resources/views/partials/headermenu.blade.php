@if (empty($submenu))
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="{{ url($lang.'/'.$menuitemVariable->slug) }}" id="navbarDropdown{{$menuitemVariable->slug}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $menuitemVariable->title }} {{-- $menuitem->subMenuTop->count().' * '.$i --}}
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown{{$menuitemVariable->slug}}">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        <li class="nav-item">
            @if (!is_null($menuitemVariable))
                @if ($menuitem->type == 'content')
                    <a href="{{ url($lang.'/'.$menuitemVariable->slug) }}" class="nav-link">
                        {{ $menuitemVariable->title }}
                    </a>
                @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                    <a class="nav-link" href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                        {{ $menuitemVariable->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @endif
            @endif
        </li>
    @endif
@else
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li class="dropdown-submenu">
            <a href="{{ url($lang.'/'.$menuitemVariable->slug) }}" class="dropdown-item dropdown-toggle">
                {{ $menuitemVariable->title }} {{-- $menuitem->subMenuTop->count().' ? '.$i --}} 
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul class="dropdown-menu @if ($i == 3) dropright-menu @endif">
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        @php $i++; @endphp
        @if (!is_null($menuitemVariable))
            <li>
                @if ($menuitem->type == 'content')
                    <a class="dropdown-item" href="{{ url($lang.'/'.$menuitemVariable->slug) }}">
                        {{ $menuitemVariable->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                    <a class="dropdown-item" href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                        {{ $menuitemVariable->title }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}
                    </a>
                @endif
            </li>
        @endif
    @endif
@endif