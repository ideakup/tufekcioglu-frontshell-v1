@if ($__asidevisible == 'yes')
	<div class="col-2 d-md-none d-lg-block d-sm-none d-none sidebar master-col-aside">
		<style type="text/css">
			.aside-menu-panel{
				padding: 0 0 0 10px;
				margin-top: 0;
    			margin-bottom: 1rem;
			}

			.aside-link{
				color: #3f3f3e;
			    padding-right: .2rem;
			    padding-left: .2rem;
			}

			#accordionMenu li{
				width: 100%;
				list-style: none;
			}
			
			.aside-menu-panel a{
				font-size: 14px;
			}

			.aside-menu-panel .dropdown-toggle{
				white-space: unset;
			}

		</style>
		<div class="panel-group" id="accordionMenu" role="tablist" aria-multiselectable="true">
			@php $pp = 0; @endphp
	    	@foreach ($topmenus as $menuitem)
				@include('partials.asidebarmenu')
    		@endforeach
	    </div>
	    
	</div>
@endif