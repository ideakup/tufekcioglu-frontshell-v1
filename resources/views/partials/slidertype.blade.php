@switch($__slidertype)
    @case("image")
        <div class="row no-margin-padding banner-bg">
            <div class="col-lg-12 col-md-12 no-margin-padding banner-style" style="background-image: url({{ env('APP_UPLOAD_PATH_V3') }}/xlarge/{{ $menu->variableLang($lang)->stvalue }});">
            </div>
        </div>
        @break

    @case("slider")

        <div id="rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="tufekcioglu" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

            <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.1">
                <ul>
                    @foreach ($menu->slider as $sElement)
                        @php
                            if (empty($sElement->variableLang($lang))) {
                                $elementVariable = $sElement->variable;
                            }else{
                                $elementVariable = $sElement->variableLang($lang);
                            }
                        @endphp
                        <li data-index="rs-{{ $sElement->id }}" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-delay="3000" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- LAYERS -->
                            
                            @if (!is_null($elementVariable->image_url))
                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption tp-resizeme" id="slide-{{ $sElement->id }}-layer-1" data-x="['right','right','center','center']" data-hoffset="['21','22','0','1']" data-y="['middle','middle','middle','middle']" data-voffset="['37','36','-62','-101']" data-fontsize="['20','20','20','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"+2500","speed":1000,"frame":"999","to":"x:50px;opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;"><img src="{{ env('APP_UPLOAD_PATH_V3') }}/xlarge/{{ $elementVariable->image_url }}" alt="" data-ww="['487px','487px','356px','310px']" data-hh="['516px','516px','379px','328px']" data-no-retina> </div>
                            @endif

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption WebProduct-Title tp-resizeme" id="slide-{{ $sElement->id }}-layer-2" data-x="['left','left','center','center']" data-hoffset="['26','26','0','0']" data-y="['middle','middle','top','middle']" data-voffset="['-185','-181','46','-338']" data-fontsize="['68','68','60','48']" data-lineheight="['80','80','75','55']" data-width="['684','684','623','527']" data-height="['164','164','155','113']" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"+3000","speed":1000,"frame":"999","to":"x:-50px;opacity:0;","ease":"Power4.easeIn"}]' data-textAlign="['left','left','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 684px; max-width: 684px; max-width: 164px; max-width: 164px; white-space: normal; font-size: 68px; line-height: 80px; font-weight: 900; color: #ffffff; letter-spacing: ;font-family:Roboto;">

                                {{ $elementVariable->title }}
                                                                
                                <p style="font-size: 16px; font-weight: 400; line-height: 24px;">&nbsp;</p>
                                <p style="font-size: 16px; font-weight: 400; line-height: 24px;">{!! $elementVariable->description !!}</p>

                            </div>

                            @if (!empty($elementVariable->button_text))
                                <div class="tp-caption rev-btn " id="slide-3-layer-17" data-x="['left','left','center','center']" data-hoffset="['26','20','0','0']" data-y="['top','top','top','middle']" data-voffset="['339','337','607','93']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"+3000","speed":1000,"frame":"999","to":"x:-50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,58,45);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[10,10,10,10]" data-paddingright="[30,30,30,30]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[30,30,30,30]" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 400; color: #000000; letter-spacing: ;font-family:Roboto;background-color:rgb(255,255,255);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="{{ $elementVariable->button_url }}">{{ $elementVariable->button_text }}</a></div>
                            @endif


                        </li>
                    @endforeach
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div><!-- END REVOLUTION SLIDER -->

        @break

    @default
        
@endswitch