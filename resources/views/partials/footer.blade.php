<footer class="page-footer footer-font">
    <div class="footer-bg">
        <div class="container">
            <div class="row py-4 d-flex align-items-center">
                <div class="col-md-9 mt-md-0 mt-3 footer-center">
                    @foreach ($topmenus as $topmenu)
                        @php
                            if (empty($topmenu->variableLang($lang))) {
                                $topmenuVariable = $topmenu->variable;
                            }else{
                                $topmenuVariable = $topmenu->variableLang($lang);
                            }
                        @endphp
                        <a href="{{ url($lang.'/'.$topmenuVariable->slug) }}" class="list-group-item-action">{{ $topmenuVariable->title }}</a>
                        @if (!$loop->last) | @endif
                    @endforeach
                </div>
                <div class="col-md-3 mb-md-0 mb-3 f-p-top-30 text-md-center footer-center">
                    @if (!empty($sitesettings->where('slug', 'social-facebook')->first()->value))
                        <a class="footer-s-link fb-ic" href="{{ $sitesettings->where('slug', 'social-facebook')->first()->value }}" target="_blank">
                            <i class="fab fa-facebook-f white-text mr-4"> </i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-twitter')->first()->value))
                        <a class="footer-s-link tw-ic" href="{{ $sitesettings->where('slug', 'social-twitter')->first()->value }}" target="_blank">
                            <i class="fab fa-twitter white-text mr-4"> </i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-linkedin')->first()->value))
                        <a class="footer-s-link li-ic" href="{{ $sitesettings->where('slug', 'social-linkedin')->first()->value }}" target="_blank">
                            <i class="fab fa-linkedin-in white-text mr-4"> </i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-instagram')->first()->value))
                        <a class="footer-s-link ins-ic" href="{{ $sitesettings->where('slug', 'social-instagram')->first()->value }}" target="_blank">
                            <i class="fab fa-instagram white-text"> </i>
                        </a>
                    @endif
                    
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row py-4 d-flex align-items-center footer-alt footer-alt">
                <div class="col-md-9 mt-md-0 mt-3 footer-center">
                    <b>{{ $sitesettings->where('slug', 'footer-address')->first()->value }}</b>
                    </br>Telefon : {{ $sitesettings->where('slug', 'footer-phone')->first()->value }} Faks : {{ $sitesettings->where('slug', 'footer-fax')->first()->value }} E-Posta : {{ $sitesettings->where('slug', 'footer-email')->first()->value }}
                </div>
                <div class="col-md-3 mb-md-0 mb-3 f-p-top-30 text-md-center footer-center">
                    <a>
                        <img src="{{ url('images/footer-logo-'.Request::segment(1).'.svg') }}" height="80" alt="">
                        <br>
                        <p class="copy-size">{{ $sitesettings->where('slug', 'copyright')->first()->value }}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>