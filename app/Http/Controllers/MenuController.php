<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\PhotoGallery;
use App\Slider;

use App;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        
    }

    public function list()
    {
        return view('menu.list');
    }

    public function view($id = null)
    {   
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus));
    }

    public function add_edit($id = null)
    {   
        $topmenus = Menu::where('deleted', 'no')->get();
        if(is_null($id)){
            $menu = new Menu();
        }else{
            $menu = Menu::find($id);
        }
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus));
    }

    public function delete($id = null)
    {   
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);
        return view('menu.crud', array('menu' => $menu, 'topmenus' => $topmenus));
    }

    public function save(Request $request)
    {   
        $text = "";
        if ($request->crud == 'add' || $request->crud == 'edit') {
            $rules = array(
                'name' => 'required|max:255',
                'order' => 'numeric|min:1|max:1000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            if($request->crud == 'add'){
                $menu = new Menu();
                
                $menu->name = $request->name;
                $menu->slug = str_slug($request->name, '-');
                if ($request->topMenu != 'null') {
                    $menu->top_id = $request->topMenu;
                }
                $menu->type = $request->type;
                $menu->link = $request->link;
                $menu->description = $request->description;
                $menu->order = $request->order;
                $menu->position = $request->position.'';
                $menu->status = ($request->status == 'active') ? 'active' : 'passive';
                $menu->save();

                $menuvariable = new MenuVariable();
                $menuvariable->menu_id = $menu->id;
                $menuvariable->title = $menu->name;
                $menuvariable->save();

                $content = new Content();
                $content->menu_id = $menu->id;
                $content->title = 'İlk İçerik';
                $content->type = 'text';
                $content->content = '';
                $content->order = 1;
                $content->save();

                $text = __('words.successadd');
            }else if($request->crud == 'edit'){
                
                $menu = Menu::find($request->id);
                $menu->name = $request->name;
                $menu->slug = str_slug($request->name, '-');
                $menu->top_id = ($request->topMenu == 'null') ? null : $request->topMenu;
                $menu->link = $request->link;
                $menu->description = $request->description;
                $menu->order = $request->order;
                $menu->position = $request->position.'';
                $menu->status = ($request->status == 'active') ? 'active' : 'passive';
                $menu->save();

                $menuvariable = $menu->setting;
                $menuvariable->title = $request->title;
                $menuvariable->headertheme = $request->headertheme.'';
                $menuvariable->slidertype = $request->slidertype.'';
                $menuvariable->breadcrumbvisible = $request->breadcrumbvisible.'';
                $menuvariable->asidevisible = $request->asidevisible.'';
                $menuvariable->save();

                $text = __('words.successedit');
            }
        }else if($request->crud == 'delete'){
            $menu = Menu::find($request->id);
            $menu->slug = null;
            $menu->deleted = 'yes';
            $menu->status = 'passive';
            $menu->save();

            $text = __('words.successdelete');
        }

        return redirect('menu/list')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function stimage($id)
    {
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);
        
        return view('menu.stimage', array('menu' => $menu, 'topmenus' => $topmenus));
    }

    public function content($id = null)
    {   
        $menu = Menu::find($id);
        if(count($menu->content) == 1){
            return redirect('menu/content/'.$menu->id.'/edit/'.$menu->content->first()->id);
        }else{
            return view('menu.content', array('menu' => $menu));
        }
    }

    public function content_add_edit($id, $cid = null)
    {   
        $menu = Menu::find($id);
        if(is_null($id)){
            $content = new Content();
        }else{
            $content = Content::find($cid);
        }
        return view('menu.contentcrud', array('menu' => $menu, 'content' => $content));
    }

    public function content_delete($id, $cid = null)
    {   
        $menu = Menu::find($id);
        $content = Content::find($cid);

        return view('menu.contentcrud', array('menu' => $menu, 'content' => $content));
    }

    public function content_save(Request $request)
    {   
        $text = "";
        if ($request->crud == 'add' || $request->crud == 'edit') {
            $rules = array(
                'title' => 'required|max:255',
                'order' => 'numeric|min:1|max:1000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            if($request->crud == 'add'){
                $content = new Content();
                
                $content->menu_id = $request->menu_id;
                $content->title = $request->title;
                if ($request->type != 'null') {
                    $content->type = $request->type.'';
                }

                $content->content = '';
                $content->order = $request->order;
                $content->status = ($request->status == 'active') ? 'active' : 'passive';
                $content->save();

                $text = __('words.successadd');
            }else if($request->crud == 'edit'){

                $content = Content::find($request->id);
                $content->title = $request->title;
                $content->order = $request->order;
                $content->status = ($request->status == 'active') ? 'active' : 'passive';
                if ($content->type == 'text' || $content->type == 'link') {
                    $content->content = $request->content;
                }
                if ($content->type == 'foto' || $content->type == 'photogallery') {
                    $content->props = $request->props;
                }else if ($content->type == 'link') {
                    $content->props = $request->props;
                }
                if ($content->type == 'text') {
                    $content->row = $request->row;
                    $content->height = $request->height;
                }else{
                    $content->row = 'normal';
                    $content->height = null;
                }

                $content->save();
                $text = __('words.successedit');
                
            }

        }else if($request->crud == 'delete'){
            
            $content = Content::find($request->id);
            $content->deleted = 'yes';
            $content->status = 'passive';
            $content->save();

            $text = __('words.successdelete');
            return redirect('menu/content/'.$content->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
            
        }
        return redirect('menu/content/'.$content->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function stslider($id = null)
    {
        $topmenus = Menu::where('deleted', 'no')->get();
        $menu = Menu::find($id);

        return view('menu.stslider', array('menu' => $menu, 'topmenus' => $topmenus));
    }

    public function stslider_add_edit($id, $sid = null)
    {   
        $menu = Menu::find($id);
        if(is_null($id)){
            $slide = new Slider();
        }else{
            $slide = Slider::find($sid);
        }
        return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide));
    }

    public function stslider_view($id, $sid = null)
    {   
        $menu = Menu::find($id);
        $slide = Slider::find($sid);
        return view('menu.stslidercrud', array('menu' => $menu, 'slide' => $slide));
    }

    public function stslider_save(Request $request)
    {   
        $text = "";
        if ($request->crud == 'add' || $request->crud == 'edit') {
            $rules = array(
                'title' => 'required|max:255',
                'order' => 'numeric|min:1|max:1000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            if($request->crud == 'add'){
                $slide = new Slider();
                
                $slide->menu_id = $request->menu_id;
                $slide->title = $request->title;
                $slide->description = $request->description;
                $slide->button_text = $request->button_text;
                $slide->button_url = $request->button_url;

                $slide->order = $request->order;
                $slide->status = ($request->status == 'active') ? 'active' : 'passive';
                $slide->save();

                $text = __('words.successadd');
            }else if($request->crud == 'edit'){

                $slide = Slider::find($request->id);
                $slide->title = $request->title;
                $slide->order = $request->order;
                $slide->status = ($request->status == 'active') ? 'active' : 'passive';
                
                $slide->description = $request->description;
                $slide->button_text = $request->button_text;
                $slide->button_url = $request->button_url;

                $slide->save();

                $text = __('words.successedit');
                
            }
        }else if($request->crud == 'delete'){
            
            $slide = Slider::find($request->id);
            $slide->deleted = 'yes';
            $slide->status = 'passive';
            $slide->save();

            $text = __('words.successdelete');
            return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
            
        }
        return redirect('menu/stslider/'.$slide->menu_id)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function photogalleryitem($id, $cid, $fid)
    {   
        $menu = Menu::find($id);
        if(is_null($id)){
            $content = new Content();
        }else{
            $content = Content::find($cid);
        }
        $photogalleryitem = PhotoGallery::find($fid);
        return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem));
    }
    
    public function photogalleryitem_delete($id, $cid, $fid)
    {   
        $menu = Menu::find($id);
        $content = Content::find($cid);
        $photogalleryitem = PhotoGallery::find($fid);
        return view('menu.photogalleryitem', array('menu' => $menu, 'content' => $content, 'photogalleryitem' => $photogalleryitem));
    }

    public function photogalleryitem_save(Request $request)
    {   
        $text = "";
        if ($request->crud == 'photogallery_edit') {
            $rules = array(
                'order' => 'numeric|min:1|max:1000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            if($request->crud == 'photogallery_edit'){

                $photogalleryitem = PhotoGallery::find($request->fid);
                $photogalleryitem->name = $request->name;
                $photogalleryitem->description = $request->description;
                $photogalleryitem->order = $request->order;
                $photogalleryitem->status = ($request->status == 'active') ? 'active' : 'passive';
                $photogalleryitem->save();
                $text = __('words.successedit');
                
            }

        }else if($request->crud == 'photogallery_delete'){
            
            $photogalleryitem = PhotoGallery::find($request->fid);
            $photogalleryitem->deleted = 'yes';
            $photogalleryitem->status = 'passive';
            $photogalleryitem->save();

            $text = __('words.successdelete');
            return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->id)->with('message', array('text' => $text, 'status' => 'success'));
            
        }
        return redirect('menu/content/'.$request->menu_id.'/edit/'.$request->id.'/photogallery_edit/'.$request->fid)->with('message', array('text' => $text, 'status' => 'success'));
    }

}
