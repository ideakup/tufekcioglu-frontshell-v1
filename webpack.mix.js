let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.copy('resources/assets/metronic/dist/default/assets/app/media/img', 'public/img');
//mix.copy('resources/assets/metronic/dist/default/assets/demo/default/media/img', 'public/img');
//mix.copy('resources/assets/metronic/dist/default/assets/vendors/base/images/', 'public/css/images');

mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');
mix.copy('resources/assets/revolution/fonts', 'public/fonts');
mix.copy('resources/assets/default_images', 'public/images');
mix.copy('node_modules/lightbox2/dist/images', 'public/images');

mix.combine([
	'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    'node_modules/@fortawesome/fontawesome-free/js/all.js',

    'node_modules/lightbox2/dist/js/lightbox.min.js',

    'resources/assets/revolution/js/jquery.themepunch.tools.min.js',
	'resources/assets/revolution/js/jquery.themepunch.revolution.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.actions.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.carousel.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.kenburn.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.migration.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.navigation.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.parallax.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.slideanims.min.js',
	'resources/assets/revolution/js/extensions/revolution.extension.video.min.js'
], 'public/js/app.js').version();


mix.combine([
    'node_modules/bootstrap/dist/css/bootstrap.css',
	'node_modules/@fortawesome/fontawesome-free/css/all.css',

	'node_modules/lightbox2/dist/css/lightbox.min.css',
	
	'resources/assets/revolution/css/settings.css',
	'resources/assets/revolution/css/layers.css',
	'resources/assets/revolution/css/navigation.css'
], 'public/css/app.css').version();

mix.combine([
    'resources/assets/custom/js/custom.js',
], 'public/js/custom.js').version();

mix.combine([
    'resources/assets/custom/css/header.css',
    'resources/assets/custom/css/slider.css',
    'resources/assets/custom/css/breadcrumb.css',
    'resources/assets/custom/css/sidebar.css',
    'resources/assets/custom/css/footer.css',
    'resources/assets/custom/css/text.css',
    'resources/assets/custom/css/general.css',
], 'public/css/custom.css').version();
